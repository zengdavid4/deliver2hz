using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotor : MonoBehaviour
{
    //public float followSpeed = 5.0f;
    private Transform lookAt;
    private Vector3 startOffset;
    //private Vector3 moveVector;

    /*private float transition = 0.0f;
    private float animationDuration = 3.0f;
    private Vector3 animationOffset = new Vector3(0,5,5);*/

    void Start()
    {
        lookAt = GameObject.Find("Player").transform;
        startOffset = transform.position - lookAt.position;

    }


    // Update is called once per frame
    void Update()
    {
        //moveVector = lookAt.position + startOffset;
        transform.position = lookAt.position + startOffset;
        
        /*
        //x
        moveVector.x = 0;
        //y
        moveVector.y = Mathf.Clamp(moveVector.y,3,5);

        if (transition > 1.0f)
        {
            transform.position = moveVector;
        }
        else
        {
            // animation at the start
            transform.position = Vector3.Lerp(moveVector + animationOffset, moveVector, transition);
            transition += Time.deltaTime * 1 / animationDuration;
            transform.LookAt(lookAt.position + Vector3.up);
        }

        

       if (player) {
        transform.position = Vector3.MoveTowards (transform.position, player.position + pos, 
        followSpeed * Time.deltaTime);
       }

       /*if (transform.position.y < 5.0f){
        transform.position = new Vector3 (transform.position.x, 4.0f, transform.position.z);
       }*/

    }

    
}

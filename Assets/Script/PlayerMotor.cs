using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    private CharacterController controller;
    private Vector3 moveVector;

    private float verticalVelocity = 0.0f;    
    private float speed = 5.0f;
    private float gravity = 09.8f; 

    //private float animationDuration = 3.0f;
    private bool isDead = false;



  
    // Start is called before the first frame update
    void Start()
    {
        controller = GetComponent<CharacterController> ();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isDead)
            return;
        
        /* 
        if(Time.time < animationDuration)
        {
            controller.Move(Vector3.forward * speed * Time.deltaTime);
            return;
        }*/

        moveVector = Vector3.zero;

        if (controller.isGrounded)
        {
            verticalVelocity = 5.0f;
        }
        else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }

        //x-left right
        moveVector.x = Input.GetAxisRaw("Horizontal") * speed;
        //y-up down
        moveVector.y = verticalVelocity;

        //z-forward back
        moveVector.z = speed;

        controller.Move(moveVector * Time.deltaTime);
    }

    //trigger next level increase speed
    public void SetSpeed(float modifier)
    {
        speed = 5.0f + modifier;
    }

    //collison
    private void OnControllerColliderHit (ControllerColliderHit hit)
    {
        if(hit.gameObject.tag == "Enemy")
            Death();
    }

    private void Death()
    {
        //Debug.Log("Dead");
        isDead = true;
        GetComponent<Score> ().OnDeath();
    }
    

}

